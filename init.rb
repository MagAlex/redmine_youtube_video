Redmine::Plugin.register :redmine_youtube_video do
	name 'YouTube macro plugin for Wiki Redmine'
	author 'MagAlex'
	description 'This plugin adds a macro to Wiki Redmine that allow the posting of YouTube videos'
	version '1.0.0'
	url 'http://magalex.ru/projects/redmine_youtube_video'
	author_url 'http://magalex.ru'

	module WikiMacros
		Redmine::WikiFormatting::Macros.register do
			desc "This plugin adds a macro to Wiki Redmine that allow the posting of YouTube videos. Syntax: <pre>{{youtube( video_key, [width, height] )}}</pre>"
			macro :redmine_youtube_video do |obj, args|
				if args.length < 1
					return ""
				end

				v = args[ 0 ]
				w = 640
				h = 480
				if args.length == 3
					w = args[ 1 ]
					h = args[ 2 ]
				end

				src = "http://www.youtube.com/embed/" + v
				content_tag( :iframe, "", :width=>w, :height=>h, :src=>src, :frameborder=>0, :allowfullscreen=>"true" )
			end
		end
	end
end
